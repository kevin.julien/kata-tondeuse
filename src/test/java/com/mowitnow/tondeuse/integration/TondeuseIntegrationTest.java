package com.mowitnow.tondeuse.integration;

import com.mowitnow.tondeuse.application.SimulationConsoleEndpoint;
import com.mowitnow.tondeuse.domain.useCase.SimulationUseCase;
import com.mowitnow.tondeuse.infrastructure.SimulationAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

public class TondeuseIntegrationTest {

    SimulationAdapter simulationAdapter = new SimulationAdapter(Path.of("src/test/resources/tondeuse-xebia.txt"));

    SimulationUseCase useCase = new SimulationUseCase(simulationAdapter);

    ByteArrayOutputStream out;

    @BeforeEach
    void setUp() {
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
    }

    @Test
    public void doit_retourner_dans_console_resultat_fichier_exemple_xebia() {
        //given
        SimulationConsoleEndpoint endpoint = new SimulationConsoleEndpoint(useCase);
        //when
        endpoint.demarrerSimulations();
        //then
        String result = format("1 3 N%s5 1 E%<s", System.lineSeparator());
        assertThat(result).isEqualTo(out.toString());
    }
}
