package com.mowitnow.tondeuse.tu.application;

import com.mowitnow.tondeuse.application.SimulationConsoleEndpoint;
import com.mowitnow.tondeuse.domain.entities.Coordonnees;
import com.mowitnow.tondeuse.domain.entities.Orientation;
import com.mowitnow.tondeuse.domain.entities.Gazon;
import com.mowitnow.tondeuse.domain.entities.Tondeuse;
import com.mowitnow.tondeuse.domain.useCase.SimulationUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SimulationEndpointTest {

    @InjectMocks
    SimulationConsoleEndpoint simulationConsoleEndpoint;

    @Mock
    SimulationUseCase simulationUseCase;

    ByteArrayOutputStream out;

    @BeforeEach
    void setUp() {
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
    }

    @Test
    void doit_afficher_console_position_finale_toutes_les_tondeuses() throws IOException {
        //given
        Tondeuse tondeuse01 = new Tondeuse(new Coordonnees(2, 0), Orientation.OUEST, new Gazon(4, 4));
        Tondeuse tondeuse02 = new Tondeuse(new Coordonnees(4, 4), Orientation.NORD, new Gazon(4, 4));
        when(simulationUseCase.demarrer()).thenReturn(List.of(tondeuse01, tondeuse02));
        //when
        simulationConsoleEndpoint.demarrerSimulations();
        //then
        String result = format("2 0 W%s4 4 N%<s", System.lineSeparator());
        assertThat(result).isEqualTo(out.toString());
    }
}