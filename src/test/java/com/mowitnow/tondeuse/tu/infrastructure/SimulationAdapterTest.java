package com.mowitnow.tondeuse.tu.infrastructure;

import com.mowitnow.tondeuse.domain.entities.*;
import com.mowitnow.tondeuse.domain.useCase.ObtenirSimulationException;
import com.mowitnow.tondeuse.infrastructure.SimulationAdapter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class SimulationAdapterTest {

    private Tondeuse tondeuseAttendue;
    private Tondeuse secondeTondeuseAttendue;

    @BeforeEach
    void setUp() {
        Gazon gazonExpected = new Gazon(104, 16);
        tondeuseAttendue = new Tondeuse(new Coordonnees(1, 2), Orientation.NORD, gazonExpected);
        secondeTondeuseAttendue = new Tondeuse(new Coordonnees(5, 5), Orientation.OUEST, gazonExpected);
    }

    @Test
    void doit_lever_ObtenirSimulationException_si_fichier_non_trouve() {
        //given
        SimulationAdapter tondeuseAdapter = new SimulationAdapter(Path.of("src/test/resources/ce-fichier-existe" +
                "-pas.txt"));
        //then
        assertThatThrownBy(tondeuseAdapter::obtenirSimulations).isInstanceOf(ObtenirSimulationException.class);
    }

    @Test
    void doit_creer_aucune_tondeuse_si_aucun_tondeuse_dans_fichier() {
        //given
        SimulationAdapter tondeuseAdapter = new SimulationAdapter(Path.of("src/test/resources/gazon-only.txt"));
        //when
        List<Simulation> simulations = tondeuseAdapter.obtenirSimulations();
        //then
        Assertions.assertThat(simulations).isNotNull().hasSize(0);
    }

    @Test
    void doit_lever_ObtenirSimulationException_si_fichier_vide() {
        //given
        SimulationAdapter tondeuseAdapter = new SimulationAdapter(Path.of("src/test/resources/no-gazon.txt"));
        //then
        assertThatThrownBy(tondeuseAdapter::obtenirSimulations).isInstanceOf(ObtenirSimulationException.class);
    }

    @Test
    void doit_creer_liste_tondeuses_avec_coordonnees_orientation_et_tondeuse() {
        //given
        Simulation simulation01Expected = new Simulation(tondeuseAttendue, "GAGAGAGAA");
        Simulation simulation02Expected = new Simulation(secondeTondeuseAttendue, "AADAADADDA");
        SimulationAdapter simulationAdapter = new SimulationAdapter(Path.of("src/test/resources/gazon-tondeuses.txt"));
        //when
        List<Simulation> simulations = simulationAdapter.obtenirSimulations();
        //then
        Assertions.assertThat(simulations).isNotNull().hasSize(2);
        assertThat(simulations.get(0)).usingRecursiveComparison().isEqualTo(simulation01Expected);
        assertThat(simulations.get(1)).usingRecursiveComparison().isEqualTo(simulation02Expected);
    }
}
