package com.mowitnow.tondeuse.tu.domain;

import com.mowitnow.tondeuse.domain.entities.*;
import com.mowitnow.tondeuse.domain.entities.Gazon;
import com.mowitnow.tondeuse.domain.useCase.SimulationPort;
import com.mowitnow.tondeuse.domain.useCase.SimulationUseCase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SimulationUseCaseTest {

    @InjectMocks
    SimulationUseCase simulationUseCase;

    @Mock
    SimulationPort simulationPort;

    @Test
    void doit_retourner_aucune_tondeuse_si_aucune_simulation() {
        //given
        when(simulationPort.obtenirSimulations()).thenReturn(Collections.emptyList());
        //when
        List<Tondeuse> tondeuses = simulationUseCase.demarrer();
        //then
        assertThat(tondeuses).isEmpty();
    }

    @Test
    void doit_retourner_liste_deux_tondeuses_aux_coordonnees_finale_si_liste_deux_tondeuses() {
        //given
        when(simulationPort.obtenirSimulations()).thenReturn(this.creerSimulationsBouchonnees());
        //when
        List<Tondeuse> tondeuses = simulationUseCase.demarrer();
        //then
        assertThat(tondeuses).isNotNull().hasSize(2);
        assertThat(tondeuses.get(0).getX()).isEqualTo(3);
        assertThat(tondeuses.get(0).getY()).isEqualTo(0);
        assertThat(tondeuses.get(0).getOrientation()).isEqualTo(Orientation.EST);
        assertThat(tondeuses.get(1).getX()).isEqualTo(6);
        assertThat(tondeuses.get(1).getY()).isEqualTo(4);
        assertThat(tondeuses.get(1).getOrientation()).isEqualTo(Orientation.NORD);
    }

    private List<Simulation> creerSimulationsBouchonnees() {
        Gazon gazon = new Gazon(10,10);
        Tondeuse tondeuse01 = new Tondeuse(new Coordonnees(3, 1), Orientation.SUD, gazon);
        Tondeuse tondeuse02 = new Tondeuse(new Coordonnees(7, 4), Orientation.OUEST, gazon);
        Simulation simulation01 = new Simulation(tondeuse01, "AAAG");
        Simulation simulation02 = new Simulation(tondeuse02, "AD");
        return List.of(simulation01, simulation02);
    }
}
