package com.mowitnow.tondeuse.tu.domain;

import com.mowitnow.tondeuse.domain.entities.Orientation;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class OrientationTest {

    @Test
    void doit_retourner_code_N_pour_orientation_NORD() {
        Assertions.assertThat(Orientation.NORD.getCode()).isEqualTo("N");
    }

    @Test
    void doit_retourner_code_S_pour_orientation_SUD() {
        assertThat(Orientation.SUD.getCode()).isEqualTo("S");
    }

    @Test
    void doit_retourner_code_W_pour_orientation_OUEST() {
        assertThat(Orientation.OUEST.getCode()).isEqualTo("W");
    }

    @Test
    void doit_retourner_code_E_pour_orientation_EST() {
        assertThat(Orientation.EST.getCode()).isEqualTo("E");
    }

    @Test
    void doit_lever_IllegalArgumentException_si_code_inconnu() {
        assertThatThrownBy(() -> Orientation.obtenirOrientationDepuisCode("Z")).isInstanceOf(IllegalArgumentException.class);
    }
}