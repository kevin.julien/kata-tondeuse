package com.mowitnow.tondeuse.tu.domain;

import com.mowitnow.tondeuse.domain.entities.Coordonnees;
import com.mowitnow.tondeuse.domain.entities.Gazon;
import com.mowitnow.tondeuse.domain.entities.Orientation;
import com.mowitnow.tondeuse.domain.entities.Tondeuse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class TondeuseTest {

    private Tondeuse tondeuse;

    @BeforeEach
    void setUp() {
        Gazon gazon = new Gazon(5,5);
        tondeuse = new Tondeuse(new Coordonnees(0, 0), Orientation.NORD, gazon);
    }

    @Test
    void doit_avoir_une_position_x_2_y_1_orientation_E_avec_valeurs_entrees() {
        //when
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(2, 1), Orientation.EST, new Gazon(2,2));
        //then
        assertThat(tondeuse.getX()).isEqualTo(2);
        assertThat(tondeuse.getY()).isEqualTo(1);
        assertThat(tondeuse.getCodeOrientation()).isEqualTo("E");
    }

    @Test
    void doit_rester_position_orientation_initiale_si_ordre_vide() {
        //when
        tondeuse.executer("");
        //then
        assertThat(tondeuse.getX()).isEqualTo(0);
        assertThat(tondeuse.getY()).isEqualTo(0);
        assertThat(tondeuse.getCodeOrientation()).isEqualTo("N");
    }

    @Test
    void doit_rester_position_orientation_initiale_si_ordre_null() {
        //when
        tondeuse.executer(null);
        //then
        assertThat(tondeuse.getX()).isEqualTo(0);
        assertThat(tondeuse.getY()).isEqualTo(0);
        assertThat(tondeuse.getCodeOrientation()).isEqualTo("N");
    }

    @ParameterizedTest
    @CsvSource({"G,W", "GG, S", "GGG, E", "GGGG, N"})
    void doit_effectuer_une_rotation_gauche(String instructions, String orientationFinale) {
        //when
        tondeuse.executer(instructions);
        //then
        assertThat(tondeuse.getCodeOrientation()).isEqualTo(orientationFinale);
    }

    @ParameterizedTest
    @CsvSource({"D,E", "DD, S", "DDD, W", "GGGG, N"})
    void doit_effectuer_une_rotation_droite(String instructions, String orientationFinale) {
        //when
        tondeuse.executer(instructions);
        //then
        assertThat(tondeuse.getCodeOrientation()).isEqualTo(orientationFinale);
    }

    @ParameterizedTest
    @CsvSource({"A,2,3", "AA,2,4"})
    void doit_avancer_y_plus_1_si_ordre_avancer_et_orientation_NORD(String ordres, int x, int y) {
        //given
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(2,2), Orientation.NORD, new Gazon(5,5));
        //when
        tondeuse.executer(ordres);
        //then
        assertThat(tondeuse.getOrientation()).isEqualTo(Orientation.NORD);
        assertThat(tondeuse.getX()).isEqualTo(x);
        assertThat(tondeuse.getY()).isEqualTo(y);
    }

    @ParameterizedTest
    @CsvSource({"A,3,2", "AA,4,2"})
    void doit_avancer_x_plus_1_si_ordre_avancer_et_orientation_EST(String ordres, int x, int y) {
        //given
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(2,2), Orientation.EST, new Gazon(5,5));
        //when
        tondeuse.executer(ordres);
        //then
        assertThat(tondeuse.getOrientation()).isEqualTo(Orientation.EST);
        assertThat(tondeuse.getX()).isEqualTo(x);
        assertThat(tondeuse.getY()).isEqualTo(y);
    }

    @ParameterizedTest
    @CsvSource({"A,2,1", "AA,2,0"})
    void doit_avancer_y_moins_1_si_ordre_avancer_et_orientation_SUD(String ordres, int x, int y) {
        //given
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(2,2), Orientation.SUD, new Gazon(5,5));
        //when
        tondeuse.executer(ordres);
        //then
        assertThat(tondeuse.getOrientation()).isEqualTo(Orientation.SUD);
        assertThat(tondeuse.getX()).isEqualTo(x);
        assertThat(tondeuse.getY()).isEqualTo(y);
    }

    @ParameterizedTest
    @CsvSource({"A,1,2", "AA,0,2"})
    void doit_avancer_x_moins_1_si_ordre_avancer_et_orientation_OUEST(String instructions, int x, int y) {
        //given
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(2,2), Orientation.OUEST, new Gazon(5,5));
        //when
        tondeuse.executer(instructions);
        //then
        assertThat(tondeuse.getOrientation()).isEqualTo(Orientation.OUEST);
        assertThat(tondeuse.getX()).isEqualTo(x);
        assertThat(tondeuse.getY()).isEqualTo(y);
    }

    @Test
    void doit_renvoyer_erreur_si_instuction_inconnue() {
        assertThatThrownBy(() -> tondeuse.executer("ZAA")).isInstanceOf(IllegalArgumentException.class);
    }

    @ParameterizedTest
    @CsvSource({"AAAA,2,3", "GAAAA,0,2", "DAAAAAAA,3,2", "DDAAAAA,2,0"})
    void doit_rester_sur_place_si_en_dehors_grille(String instructions, int x, int y) {
        //given
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(2,2), Orientation.NORD, new Gazon(3,3));
        //when
        tondeuse.executer(instructions);
        //then
        assertThat(tondeuse.getX()).isEqualTo(x);
        assertThat(tondeuse.getY()).isEqualTo(y);
    }
}
