package com.mowitnow.tondeuse.domain.useCase;

import com.mowitnow.tondeuse.domain.entities.Simulation;

import java.util.List;

public interface SimulationPort {

    List<Simulation> obtenirSimulations();
}
