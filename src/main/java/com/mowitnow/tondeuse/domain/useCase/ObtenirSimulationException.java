package com.mowitnow.tondeuse.domain.useCase;

public class ObtenirSimulationException extends RuntimeException {

    public ObtenirSimulationException(String message) {
        super(message);
    }

    public ObtenirSimulationException(String message, Throwable cause) {
        super(message, cause);
    }
}
