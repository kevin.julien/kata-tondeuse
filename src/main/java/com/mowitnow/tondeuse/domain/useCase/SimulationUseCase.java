package com.mowitnow.tondeuse.domain.useCase;

import com.mowitnow.tondeuse.domain.entities.Tondeuse;
import com.mowitnow.tondeuse.domain.entities.Simulation;

import java.util.List;
import java.util.stream.Collectors;

public class SimulationUseCase {

    private final SimulationPort simulationPort;

    public SimulationUseCase(SimulationPort simulationPort) {
        this.simulationPort = simulationPort;
    }

    public List<Tondeuse> demarrer() {
        List<Simulation> simulations = this.simulationPort.obtenirSimulations();
        return simulations.stream()
                .map(simulation -> simulation.getTondeuse().executer(simulation.getInstructions()))
                .collect(Collectors.toList());
    }
}
