package com.mowitnow.tondeuse.domain.entities;

import java.util.Arrays;

public enum Orientation {

    NORD("N", "W", "E"),
    EST("E", "N", "S"),
    SUD("S", "E", "W"),
    OUEST("W", "S", "N");

    private final String code;
    private final String codeOrientationGauche;
    private final String codeOrientationDroite;

    Orientation(String code, String codeOrientationGauche, String codeOrientationDroite) {
        this.code = code;
        this.codeOrientationGauche = codeOrientationGauche;
        this.codeOrientationDroite = codeOrientationDroite;
    }

    public String getCode() {
        return code;
    }

    public Orientation tournerAGauche() {
        return obtenirOrientationDepuisCode(this.codeOrientationGauche);
    }

    public Orientation tournerADroite() {
        return obtenirOrientationDepuisCode(this.codeOrientationDroite);
    }

    public static Orientation obtenirOrientationDepuisCode(String code) {
        return Arrays.stream(Orientation.values())
                .filter(o -> o.code.equals(code))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("Impossible de trouver l'orientation associée" +
                        " au " +
                        "code suivant : " + code));
    }
}
