package com.mowitnow.tondeuse.domain.entities;

public class Gazon {

    private final int largeur;

    private final int hauteur;

    public Gazon(int largeur, int hauteur) {
        this.largeur = largeur;
        this.hauteur = hauteur;
    }

    public Coordonnees calculerProchaineCoordonnees(Coordonnees coordonnees, Orientation orientation) {
        if (Orientation.NORD.equals(orientation) && estEnDessousHauteurMaximale(coordonnees)) {
            return new Coordonnees(coordonnees.getX(), coordonnees.getY() + 1);
        } else if (Orientation.EST.equals(orientation) && estEnDessousLargeurMaximale(coordonnees)) {
            return new Coordonnees(coordonnees.getX() + 1, coordonnees.getY());
        } else if (Orientation.SUD.equals(orientation) && estAuDessusHauteurMinimale(coordonnees)) {
            return new Coordonnees(coordonnees.getX(), coordonnees.getY() - 1);
        } else if (Orientation.OUEST.equals(orientation) && estAuDessusLargeurMinimale(coordonnees)) {
            return new Coordonnees(coordonnees.getX() - 1, coordonnees.getY());
        }
        return coordonnees;
    }

    private boolean estAuDessusLargeurMinimale(Coordonnees coordonnees) {
        return coordonnees.getX() - 1 >= 0;
    }

    private boolean estEnDessousLargeurMaximale(Coordonnees coordonnees) {
        return coordonnees.getX() + 1 <= largeur;
    }

    private boolean estAuDessusHauteurMinimale(Coordonnees coordonnees) {
        return coordonnees.getY() - 1 >= 0;
    }

    private boolean estEnDessousHauteurMaximale(Coordonnees coordonnees) {
        return coordonnees.getY() + 1 <= hauteur;
    }

}
