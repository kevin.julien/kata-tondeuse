package com.mowitnow.tondeuse.domain.entities;

public class Simulation {

    private final Tondeuse tondeuse;

    private final String instructions;

    public Simulation(Tondeuse tondeuse, String instructions) {
        this.tondeuse = tondeuse;
        this.instructions = instructions;
    }

    public Tondeuse getTondeuse() {
        return tondeuse;
    }

    public String getInstructions() {
        return instructions;
    }
}
