package com.mowitnow.tondeuse.domain.entities;

public class Tondeuse {

    private Coordonnees coordonnees;
    private Orientation orientation;
    private final Gazon gazon;

    public Tondeuse(Coordonnees coordonnees, Orientation orientation, Gazon gazon) {
        this.coordonnees = coordonnees;
        this.orientation = orientation;
        this.gazon = gazon;
    }

    public int getX() {
        return this.coordonnees.getX();
    }

    public int getY() {
        return this.coordonnees.getY();
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public String getCodeOrientation() {
        return orientation.getCode();
    }

    public Tondeuse executer(String instructions) {
        if (instructions != null) {
            instructions.trim()
                    .chars()
                    .forEach(this::choisirCommande);
        }
        return this;
    }

    private void choisirCommande(int instruction) {
        switch (instruction) {
            case 'G':
                this.orientation = orientation.tournerAGauche();
                break;
            case 'D':
                this.orientation = orientation.tournerADroite();
                break;
            case 'A':
                this.coordonnees = gazon.calculerProchaineCoordonnees(coordonnees, orientation);
                break;
            default:
                String message = String.format("L'instruction %c n'est pas gérée par ce modèle de " +
                        "tondeuse", instruction);
                throw new IllegalArgumentException(message);
        }
    }
}
