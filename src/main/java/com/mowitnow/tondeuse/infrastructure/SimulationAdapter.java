package com.mowitnow.tondeuse.infrastructure;

import com.mowitnow.tondeuse.domain.entities.*;
import com.mowitnow.tondeuse.domain.useCase.ObtenirSimulationException;
import com.mowitnow.tondeuse.domain.useCase.SimulationPort;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.String.format;
import static java.util.stream.Collectors.groupingBy;

public class SimulationAdapter implements SimulationPort {

    public static final String SEPARATEUR_INFORMATION_FICHIER = " ";
    private final Path cheminFichier;

    public SimulationAdapter(Path cheminFichier) {
        this.cheminFichier = cheminFichier;
    }

    @Override
    public List<Simulation> obtenirSimulations() {
        try {
            List<String> lignesFichier = Files.readAllLines(cheminFichier);
            if (lignesFichier.isEmpty()) {
                throw new ObtenirSimulationException(format("Impossible de créer des tondeuses avec le fichier " +
                        "fournit : %s", cheminFichier));
            }
            Gazon gazon = this.creerGazon(lignesFichier);
            return recupererInformationsGrouperParTondeuse(lignesFichier)
                    .stream()
                    .map(tondeuse -> creerSimulation(gazon, tondeuse))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new ObtenirSimulationException(format("Erreur lors de la lecture du fichier %s contenant les " +
                    "informations des tondeuses", cheminFichier), e);
        }
    }

    private Collection<List<String>> recupererInformationsGrouperParTondeuse(List<String> lignesFichier) {
        final AtomicInteger counter = new AtomicInteger(0);
        return IntStream.range(1, lignesFichier.size())
                .mapToObj(lignesFichier::get)
                .collect(groupingBy(it -> counter.getAndIncrement() / 2))
                .values();
    }

    private Simulation creerSimulation(Gazon gazon, List<String> informations) {
        String[] informationsTondeuse = informations.get(0).split(SEPARATEUR_INFORMATION_FICHIER);
        String instructions = informations.get(1);
        int abscisseTondeuse = Integer.parseInt(informationsTondeuse[0]);
        int ordonneeTondeuse = Integer.parseInt(informationsTondeuse[1]);
        String codeOrientation = informationsTondeuse[2];
        Tondeuse tondeuse = new Tondeuse(
                new Coordonnees(abscisseTondeuse, ordonneeTondeuse),
                Orientation.obtenirOrientationDepuisCode(codeOrientation),
                gazon);
        return new Simulation(tondeuse, instructions);
    }

    private Gazon creerGazon(List<String> lignesFichier) {
        String ligneGazon = lignesFichier.get(0);
        return new Gazon(getLargeur(ligneGazon), getHauteur(ligneGazon));
    }

    private Integer getLargeur(String ligneGazon) {
        return Integer.parseInt(ligneGazon.split(SEPARATEUR_INFORMATION_FICHIER)[0]);
    }

    private Integer getHauteur(String ligneGazon) {
        return Integer.parseInt(ligneGazon.split(SEPARATEUR_INFORMATION_FICHIER)[1]);
    }
}
