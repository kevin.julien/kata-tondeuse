package com.mowitnow.tondeuse.application;

import com.mowitnow.tondeuse.domain.useCase.SimulationUseCase;

public class SimulationConsoleEndpoint {

    private final SimulationUseCase simulationUseCase;

    public SimulationConsoleEndpoint(SimulationUseCase simulationUseCase) {
        this.simulationUseCase = simulationUseCase;
    }

    public void demarrerSimulations() {
        this.simulationUseCase.demarrer()
                .forEach(tondeuse -> {
                    String message = String.format("%s %s %s", tondeuse.getX(), tondeuse.getY(),
                            tondeuse.getCodeOrientation());
                    System.out.println(message);
                });
    }
}
