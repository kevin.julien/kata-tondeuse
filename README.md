# Kata Tondeuse

## Prérequis

Avoir sur son poste : 
* Java 11+
* Maven 3+

## Stack technique
* Java 11
* Tests : 
  * Junit 5
  * Mockito
  * AssertJ

## Choix techniques 

### Langue
Utilisation du français 
### Démarrage de l'application
Pas de classe Main. 
Exécution du programme complet via le test `src/test/java/integration/TondeuseIntegrationTest`
### Organisation du code
Volonté de partir sur une architecture hexagonale (même si jamais pratiqué sur un projet pro) permettant : 
* d'isoler les règles métier liées à la tondeuse automatique (tondeuse, gazon...)
* de récupérer les informations nécéssaires au domaine métier
    * Isoler dans un package nommé "infrastructure" toute la complexité liée à la gestion de fichier
* de solliciter le domaine métier
  * Ici on affiche les informations du domaine dans la console
  * Isoler dans un package nommé "application"
    * Permet de facilement créer d'autres clients (XML, JSON...) sans toucher au coeur métier
### Organisation des tests
Les tests dans `src/test/java/tu` concernent les tests unitaires : 
* Utilisation de mocks lors d'appels à d'autres classes

Les tests dans `src/test/java/integration` concernent les tests d'intégration :
* Pas de mock
* Sert à tester de bout en bout le programme
* Le test prend en entrée le fichier proposé dans l'énoncé de l'exercice
### Pourquoi ne pas utiliser Spring ?
Pas d'utilité à utiliser ce framework dans le cadre de cette problématique.


